import gzip
import pickle
import numpy as np
import csv
import os
import time
import random


def convert_number_digits(number, limit):
    target = np.eye(10)

    cifre = [int(c) for c in str(number)]

    while len(cifre) < limit:
        cifre.insert(0,0)

    seq = [target[cifre[i]] for i in range(limit)]
    return np.concatenate(seq)


class Mnist_digit:
    def __init__(self):
        f = gzip.open('mnist.pkl.gz', 'rb')
        self.train_set, self.valid_set, self.test_set = pickle.load(f, encoding="bytes")
        f.close()

    def get_raw_data(self):
        return self.train_set, self.valid_set, self.test_set

    def get_validation_data(self):
        target = np.eye(10)

        train_x_reshaped, valid_x_reshaped, test_x =  map(lambda x : x.reshape(x.shape[0], 1, -1), [self.train_set[0], self.valid_set[0], self.test_set[0]])
        train_x = np.concatenate((train_x_reshaped,valid_x_reshaped))

        train_y_reshaped, valid_y_reshaped, test_y =  map(lambda x : target[x].reshape(x.shape[0], 1, -1), [self.train_set[1], self.valid_set[1], self.test_set[1]])
        train_y = np.concatenate((train_y_reshaped,valid_y_reshaped))

        return (train_x, train_y), (test_x, test_y)

class FacebookComments:
    def __init__(self):
        self.train_variants = []

        self.maxime = [0 for _ in range(54)]
        self.minime = [0 for _ in range(54)]

        for i in range(1, 2):
            with open('./Training/Features_Variant_{}.csv'.format(i)) as f:
                reader = csv.reader(f, delimiter=',')
                lista_atribute = []
                lista_output = []

                for row in reader:
                    intreg = list(map(float, row))
                    self.maxime = [max(self.maxime[i],intreg[i]) for i in range(54)]
                    self.minime = [min(self.minime[i],intreg[i]) for i in range(54)]
                    lista_atribute.append(np.array(intreg[:-1]))
                    lista_output.append(np.array(intreg[-1]))
                self.train_variants.append([np.array(lista_atribute), np.array(lista_output)])
                
        self.test_cases = []        

        for i in range(1, 11):
            with open('./Testing/TestSet/Test_Case_{}.csv'.format(i)) as f:
                reader = csv.reader(f, delimiter=',')
                lista_atribute = []
                lista_output = []

                for row in reader:
                    intreg = list(map(float, row))
                    self.maxime = [max(self.maxime[i],intreg[i]) for i in range(54)]
                    self.minime = [min(self.minime[i],intreg[i]) for i in range(54)]
                    lista_atribute.append(np.array(intreg[:-1]))
                    lista_output.append(np.array(intreg[-1]))

                self.test_cases.append([np.array(lista_atribute), np.array(lista_output)])


        
        with open('./Testing/Features_TestSet.csv') as f:
            reader = csv.reader(f, delimiter=',')
            lista_atribute = []
            lista_output = []

            for row in reader:
                intreg = list(map(float, row))
                self.maxime = [max(self.maxime[i],intreg[i]) for i in range(54)]
                self.minime = [min(self.minime[i],intreg[i]) for i in range(54)]
                lista_atribute.append(np.array(intreg[:-1]))
                lista_output.append(np.array(intreg[-1]))


            self.features_test_set = [np.array(lista_atribute), np.array(lista_output)]


        #for i in range(53):
        #    print(i+1, self.maxime[i], self.minime[i])

    def normalize_row(self, row):
        for i in range(39):
            if i == 37:
                continue
            row[i] = (row[i]-self.minime[i])/(self.maxime[i]-self.minime[i])

    def normalize_data(self):
        for var in self.train_variants:
            for row in var[0]:
                self.normalize_row(row)

        for case in self.test_cases:
            for row in case[0]:
                self.normalize_row(row)

        for row in self.features_test_set[0]:
            self.normalize_row(row)

    def get_validation_data(self, variant_number=1, test_case=None):
        self.normalize_data()
        nr_cifre = len(str(int(self.maxime[53])))


        print(self.train_variants[0][1].shape)
        train_x, test_x = map(lambda x : x.reshape(x.shape[0], 1, -1), [self.train_variants[variant_number-1][0], \
                            self.features_test_set[0] if test_case is None else self.test_cases[test_case-1][0]])
        print(train_x.shape)

        print(self.train_variants[0][1], type(self.train_variants[0][1]))

        a = map(lambda x : x+2, self.train_variants[0][1])
        print(type(a))

        train_y, test_y = map(lambda x : convert_number_digits(int(x), nr_cifre), [self.train_variants[variant_number-1][1], \
                            self.features_test_set[1] if test_case is None else self.test_cases[test_case-1][1]])
        new_train_set = [[x.reshape(x.shape[0], 1, -1) for x in self.train_variants[variant_number-1][0]],
                        [convert_number_digits(int(x), nr_cifre) for x in self.train_variants[variant_number-1][1]]]
        print(train_y.shape)


        if test_case is None:
            new_test_set = [[x.reshape(-1, x.shape[0]) for x in self.features_test_set[0]],
                         [convert_number_digits(int(x), nr_cifre) for x in self.features_test_set[1]]]
        else:
            new_test_set = [[x.reshape(-1, x.shape[0]) for x in self.test_cases[test_case-1][0]],
                         [convert_number_digits(int(x), nr_cifre) for x in self.test_cases[test_case-1]]]
            
        return new_train_set, new_test_set
        
     

class Polimerization:
    def __init__(self):
        self.data = np.genfromtxt(os.path.join('datafiles','Polimerization_BPODataset.csv'), delimiter=',', skip_header=1)
        
        self.nr_date = self.data.shape[0]
        self.split_data()

    def get_raw_data(self):
        return self.data

    def normalize_data(self):
        maxim_list = np.amax(self.inputs, axis=0)
        minim_list = np.amin(self.inputs, axis=0)
        diferente_list = maxim_list-minim_list
        
        self.inputs = (self.inputs - minim_list)/diferente_list
        

    def split_data(self):
        self.inputs = np.reshape(self.data[...,:4], (self.data.shape[0], 1, -1))
        self.output_x = self.data[...,4]
        self.output_mn = self.data[...,5]
        self.output_mw = self.data[...,6]

    def get_validation_data(self):
        self.normalize_data()
        return ((self.inputs[:40000], self.output_x[:40000], self.output_mn[:40000], self.output_mw[:40000]),
            (self.inputs[40000:], self.output_x[40000:], self.output_mn[40000:], self.output_mw[40000:]))

    def get_crossvalidation_data(self, k=10):
        # k fold

        self.normalize_data()

        nr_date = self.data.shape[0]
        intregi = list(range(nr_date))
        random.shuffle(intregi)

        k_partitii = [[(self.inputs[i], self.output_x[i], self.output_mw[i], self.output_mn[i]) \
                for i in intregi[j:j+k]] \
                for j in range(0, self.nr_date, k)]

        return k_partitii




        
        


