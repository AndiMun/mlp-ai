import re
import inspect
from activation import activation_dict

"""
Straturi ale retelei neuronale
Un strat contine:
    - numarul de elemente din strat
    - functia de activare
    - functia de initializare a weighturilor
    - functia de initializare a biasurilor
"""

class Layer:
    def __init__(self, no_units, activation, weight_initializer='lecun_normal', bias_initializer='zeros', l2_regularizer=0.0):
        self.no_units = no_units
        self.activation = activation_dict['softmax' if activation not in activation_dict else activation]
        self.weight_initializer, self.weight_params = parse_function(weight_initializer)
        self.bias_initializer, self.bias_params = parse_function(bias_initializer)
        self.l2_regularizer = l2_regularizer


def parse_function(function_string):
    """
        functie de parsare a parametrilor weight_initalizer, bias_initializer
        input: string
        output: tuplu format din referinta la functie si un dictionar cu argumentele functiei
    """

    obj = re.match(pattern=r'(\w+)(\((([ ]*\w+[ ]*=[ ]*\d+[ ]*,)*[\w ]+=[\d ]+)\)){0,1}', string=function_string)
    if obj is None:
        return None

    nume_functie = obj.group(1)
    parametrii = obj.group(3)

    if parametrii is None:
        return (nume_functie, dict())

    params_dict = dict()

    for p in parametrii.replace(' ', '').split(','):
        p_split = p.split('=')
        params_dict[p_split[0]] = p_split[1]
    
    return (nume_functie, dict(map(lambda x : x.split('='), parametrii.replace(' ', '').split(','))))
