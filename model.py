import numpy as np

"""
Modelul retelei neuronale
la care se pot adauga straturi
"""

class Model:
    def __init__(self):
        self.layers = []

    def add_layer(self, layer):
        self.layers.append(layer)

    def compile(self, optimizer, loss, comparing_function):
        self.optimizer = optimizer

        self.optimizer.update(self.layers, loss, comparing_function)

    def fit(self, train, validation, no_epochs=1, minibatch_size = 30, metrics=['acc']):
        return self.optimizer.train(train, validation, no_epochs, minibatch_size, metrics)