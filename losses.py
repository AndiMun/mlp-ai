import numpy as np

def mse(y, target):
    return ((target - y)*(target - y))/2

def mse_derivative(y, target):
    return y-target

def cross_entropy(y, target):
    return (-1)*(target*np.log(y, where= y > 0)+(1-target)*np.log(1-y, where = 1-y > 0))

def cross_entropy_derivative(y, target):
    numitor = y*(1-y)

    #if 0 in numitor:
    #   numitor += (np.min(y))/2
    
    # print(0 in numitor)
    return (y-target)/numitor

delta_cross = {
    "sigmoid" : lambda activ_x, target : activ_x - target,
    "softmax" : lambda activ_x, target : activ_x - target,
    "linear" : cross_entropy_derivative
}

delta_mse = {
    "sigmoid" : lambda activ_x, target : (activ_x - target)*activ_x*(1-activ_x),
    "softmax" : lambda activ_x, target : (activ_x - target)*activ_x*(1-activ_x),
    "linear" : mse_derivative
}

losses_dict = {
    'mse': (mse, mse_derivative, delta_mse),
    'cross-entropy': (cross_entropy, cross_entropy_derivative, delta_cross)
}