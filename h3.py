import numpy
import random
import pickle, gzip

def sigmoid(x):
    return 1 / (1 + numpy.exp(-x))

def softmax(x):
    return numpy.exp(x) / sum(numpy.exp(x[0]))

def propagate(x, w, b):
    return numpy.dot(x, w) + b

def initialize_gradient(a):
    ret_list = []
    for i in range(len(a)):
        ret_list.append(numpy.zeros(numpy.shape(a[i])))
    return ret_list
    
def update_values_with_gradient(a, delta_a):
    ret_list = []
    for i in range(len(a)):
        ret_list.append(a[i] - delta_a[i])
    return ret_list

def forward_feed(network, x, y, z, layer_length):
    w, b = network
    y[0] = x[None, :]
    for j in range(len(layer_length) - 1):
        z[j] = propagate(y[j], w[j], b[j])
        y[j + 1] = sigmoid(z[j])
    return y, z
    





def initialize_network(*args):
    if len(args) == 1:
        print("s-au introdus cele 4 argumente!")
        layer_length = args[0]
    else:
        print("nu s-au introdus cele 4 argumente!")
        input_layer = int(input("introduceti numarul de date de intrare in retea: "))
        output_layer = int(input("introduceti numarul de date de iesire din retea: "))
        hidden_layers = int(input("introduceti numarul de straturi interioare (ascunse): "))
        layer_length = [input_layer]
        for i in range(hidden_layers):
            layer_length.append(int(input("introduceti numarul de neuroni de pe stratul interior numarul %s: " % (i + 1))))
        layer_length.append(output_layer)
    w = []
    b = []

    for i in range(len(layer_length) - 1):
        w.append(numpy.random.randn(layer_length[i], layer_length[i + 1]) / (layer_length[i] ** (1 / 2)))
        b.append(numpy.zeros(layer_length[i + 1]))

    return w, b

def initialize_aux(network):
    w, b = network
    layer_length = [numpy.shape(i)[0] for i in w]
    layer_length.append(numpy.shape(w[-1])[1])
    return [None] * (len(b) + 1), [None] * len(b), [None] * len(b), layer_length

def MSE_backpropagation_algorithm(train_set, validation_set, network, nr_of_iterations):
    w, b = network
    y, z, err, layer_length = initialize_aux(network)
    delta_w = initialize_gradient(w)
    delta_b = initialize_gradient(b)

    for k in range(nr_of_iterations):
        mb = 0
        for i in train_set:
            t = numpy.zeros(layer_length[-1])
            t[i[1]] = 1
            y[0] = i[0][None, :]
            for j in range(len(layer_length) - 1):
                z[j] = propagate(y[j], w[j], b[j])
                y[j + 1] = sigmoid(z[j])

        
            err[-1] = y[-1] * (1 - y[-1]) * (y[-1] - t)
            delta_w[-1] = delta_w[-1] + 0.2 * numpy.dot(numpy.transpose(y[-2]), err[-1])
            delta_b[-1] = delta_b[-1] + 0.2 * err[-1]
            for j in range(len(layer_length) - 2, 0, -1):
                err[j - 1] = y[j] * (1 - y[j]) * numpy.dot(err[j], numpy.transpose(w[j]))
                delta_w[j - 1] = delta_w[j - 1] + 0.2 * numpy.dot(numpy.transpose(y[j - 1]), err[j - 1])
                delta_b[j - 1] = delta_b[j - 1] + 0.2 * err[j - 1]

            if mb == 10:
                w = update_values_with_gradient(w, delta_w)
                b = update_values_with_gradient(b, delta_b)
                delta_w = initialize_gradient(w)
                delta_b = initialize_gradient(b)
                mb = 0
            else:
                mb += 1
        print("s-a terminat epoca nr %s de antrenat" % (k + 1))
        print("procentajul obtinut pe setul de validare este: %s" % (test(network, validation_set)))

    return w, b

def cross_entropy_BKP_algorithm(train_set, validation_set, network, nr_of_iterations):
    w, b = network
    y, z, err, layer_length = initialize_aux(network)
    delta_w = initialize_gradient(w)
    delta_b = initialize_gradient(b)

    for k in range(nr_of_iterations):
        mb = 0
        for i in train_set:
            t = numpy.zeros(layer_length[-1])
            t[i[1]] = 1
            y[0] = i[0][None, :]
            for j in range(len(layer_length) - 1):
                z[j] = propagate(y[j], w[j], b[j])
                y[j + 1] = sigmoid(z[j])
            y[-1] = softmax(z[-1])
            
            err[-1] = (y[-1] - t)
            delta_w[-1] = delta_w[-1] + 0.3 * numpy.dot(numpy.transpose(y[-2]), err[-1])
            delta_b[-1] = delta_b[-1] + 0.3 * err[-1]
            for j in range(len(layer_length) - 2, 0, -1):
                err[j - 1] = (y[j] * (1 - y[j]) * numpy.dot(err[j], numpy.transpose(w[j])))
                delta_w[j - 1] = delta_w[j - 1] + 0.3 * numpy.dot(numpy.transpose(y[j - 1]), err[j - 1])
                delta_b[j - 1] = delta_b[j - 1] + 0.3 * err[j - 1]

            if mb == 10:
                w = update_values_with_gradient(w, delta_w)
                b = update_values_with_gradient(b, delta_b)
                delta_w = initialize_gradient(w)
                delta_b = initialize_gradient(b)
                mb = 0
            else:
                mb += 1
        print("s-a terminat epoca nr %s de antrenat" % (k + 1))
        network = (w, b)
        print("procentajul obtinut pe setul de validare este: %s" % (test(network, validation_set)))

    return w, b
    
    

def test(network, test_set):
    w, b = network
    y, z, err, layer_length = initialize_aux(network)
    count = 0
    for i in test_set:
        t = numpy.zeros(layer_length[-1])
        t[i[1]] = 1
        y[0] = i[0][None, :]
        for j in range(len(layer_length) - 1):
            z[j] = propagate(y[j], w[j], b[j])
            y[j + 1] = sigmoid(z[j])

        max = 0
        
        for j in range(10):
            if (y[-1])[0][j] > max:
                ind = j
                max = y[-1][0][j]

        y[-1] = numpy.zeros(10)
        y[-1][ind] = 1

        if numpy.array_equal(y[-1], t):
            count = count + 1
    return 100 * (count / len(test_set))
        







if __name__ == '__main__':

    f = gzip.open("mnist.pkl.gz", "rb")
    train_set, valid_set, test_set = pickle.load(f, encoding="latin1")
    f.close()

    train_set = list(zip(train_set[0], train_set[1]))
    valid_set = list(zip(valid_set[0], valid_set[1]))
    test_set = list(zip(test_set[0], test_set[1]))

    network = initialize_network([784, 100, 70, 31, 10])
    #network = MSE_backpropagation_algorithm(train_set, valid_set, network, 1)
    network = cross_entropy_BKP_algorithm(train_set, valid_set, network, 30)
    print(test(network, test_set))



    test(network, test_set)

