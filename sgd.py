from initializers import initializer_dict
from losses import losses_dict
import numpy as np
import random
import time
import losses

class SGD:
    """
    SGD (Stochastic Gradient Descent)
    variabile:
        - learning rate
        - momentum
    """

    def __init__(self, lr=0.1, momentum=0.0, nesterov=False, adaptive_lr=False):
        self.lr = lr
        self.momentum = momentum
        self.nesterov = nesterov
        self.adaptive_lr = adaptive_lr
        self.z = []
        self.y = []


    def update(self, layers, loss, comparing_function):
        """
        se actualizeaza optimizatorul cu straturile retelei
        se initializeaza weighturile si biasurile
        se actualizeaza si functia de cost (pierdere)
        """
        
        self.weights = []
        self.biases = []
        self.layers = layers
        self.loss = losses_dict[loss]
        self.no_layers = len(layers)
        self.l2_regularizers = [l.l2_regularizer for l in layers]
        self.comparing_function = comparing_function

        if 1.0 >= self.momentum > 0.0:
            self.prec_weights = []
            self.prec_biases = []

        for i in range(1, self.no_layers):
            layers[i].weight_params["fan_in"] = layers[i-1].no_units
            layers[i].weight_params["fan_out"] = layers[i].no_units
            self.weights.append(initializer_dict[layers[i].weight_initializer](**layers[i].weight_params))
            
            layers[i].bias_params["fan_in"] = 1
            layers[i].bias_params["fan_out"] = layers[i].no_units
            self.biases.append(initializer_dict[layers[i].bias_initializer](**layers[i].bias_params))

        self.delta_L_func = self.loss[2].get(layers[-1].activation[0].__name__, None)
        #print(len(self.biases), self.biases)

    def feed_forward(self, inp):
        """
        pasul de feed forwarding
        y[-1] contine outputul retelei
        """

        self.z.clear()
        self.y.clear()

        self.y.append(inp)
        for i in range(self.no_layers):
            self.z.append(np.dot(self.y[i], self.weights[i]) + self.biases[i])
            self.y.append(self.layers[i].activation[0](self.z[i]))
        
        

    def faster_check(self, inp):
        """
        functie asemanatoare feed_forward, fara a stoca rezultatele de pe celelalte straturi
        """
        y = inp

        for i in range(self.no_layers):
            z = np.dot(y, self.weights[i]) + self.biases[i]
            y = self.layers[i].activation[0](z)

        return y


    def backpropagation(self, t):
        """
        pasul de backpropagation
        delta reprezinta modificarea care trebuie aplicata pentru scaderea costului
        se propaga eroarea pentru a actualiza diferentele cu care trebuie modificate
        weighturile si biasurile
        """
        # eroare = self.y[-1] - t

        delta = self.loss[1](self.y[-1], t)*self.layers[-1].activation[1](self.z[-1]) if self.delta_L_func is None \
            else self.delta_L_func(self.y[-1],t)#*self.layers[-1].activation[1](self.z[-1])
        # print(delta.shape, self.y[-2].shape)
        self.dif_biases[-1] += delta   
        self.dif_weights[-1] += self.y[-2].T.dot(delta) 
        for i in range(len(self.dif_biases)-2, -1, -1):
            delta = (delta.dot(self.weights[i+1].T))*self.layers[i].activation[1](self.z[i])
            self.dif_biases[i] += delta
            self.dif_weights[i] += self.y[i].T.dot(delta)

    def apply_changes(self, minibatch_size, nr_instante_antrenare):
        """
        se modifica biasurile si weighturile in functie de diferentele calculate
        in pasul de backpropagation
        """
        for i in range(len(self.biases)):
            if 1.0 >= self.momentum > 0.0:
                # l2 regularizare
                self.weights[i] = (1-self.l2_regularizers[i]*self.lr / nr_instante_antrenare)*self.weights[i]
                
                if self.nesterov:
                    self.weights[i] -= self.momentum*self.prec_dif_weights[i]
                    self.biases[i] -= self.momentum*self.prec_dif_biases[i]

                self.prec_dif_biases[i] = self.momentum*self.prec_dif_biases[i] - self.dif_biases[i] * self.lr / minibatch_size
                self.prec_dif_weights[i] = self.momentum*self.prec_dif_weights[i] - self.dif_weights[i] * self.lr / minibatch_size
                self.biases[i] =  self.biases[i] + self.prec_dif_biases[i]*(1 + self.momentum * int(self.nesterov))
                self.weights[i] += self.prec_dif_weights[i]*(1 + self.momentum * int(self.nesterov))
            else:
                self.biases[i] = self.biases[i] -self.dif_biases[i] * self.lr / minibatch_size
                self.weights[i] = (1-self.l2_regularizers[i]*self.lr / nr_instante_antrenare)*self.weights[i] - self.dif_weights[i] * self.lr / minibatch_size

    def calculate_error(self, minibatch):
        error = 0
        for x, c in minibatch:
            y = self.faster_check(x)
            error += losses.cross_entropy(c, y)
        return sum(error / len(y))

    def check_accuracy(self, inp, target):
        """
        verificarea acuratetei
        """
        no_validation_instances = len(inp)
        correct = 0
        
        for i in range(no_validation_instances):
            y = self.faster_check(inp[i])
            if self.comparing_function(y) == self.comparing_function(target[i]):
                correct += 1


        return 100*correct / no_validation_instances 

    def train(self, train, validation, no_epochs, minibatch_size, metrics):
        
        self.layers[0].weight_params["fan_in"] = train[0][0].shape[-1]
        self.layers[0].weight_params["fan_out"] = self.layers[0].no_units
        self.weights.insert(0,initializer_dict[self.layers[0].weight_initializer](**self.layers[0].weight_params))

        self.layers[0].bias_params["fan_in"] = 1
        self.layers[0].bias_params["fan_out"] = self.layers[0].no_units
        self.biases.insert(0,initializer_dict[self.layers[0].bias_initializer](**self.layers[0].bias_params))

        if 1.0 >= self.momentum > 0.0:
            self.prec_dif_biases = [np.zeros(b.shape) for b in self.biases]
            self.prec_dif_weights = [np.zeros(w.shape) for w in self.weights]
        nr_instante_antrenare = len(train[0])


        pierderi = []
        acuratete = []

        for epoch in range(no_epochs):
            acum = time.time()
            print("Epoca", epoch)

            lista_indecsi = list(range(0, nr_instante_antrenare))
            random.shuffle(lista_indecsi)

            minibatchuri = [[(train[0][i], train[1][i]) \
                for i in lista_indecsi[j:j+minibatch_size]] \
                for j in range(0, nr_instante_antrenare, minibatch_size)]

            nr_minibatchuri = len(minibatchuri)

            eroare = 0
            for minibatch in minibatchuri:
                e = 0
                self.dif_biases = [np.zeros(b.shape) for b in self.biases]
                self.dif_weights = [np.zeros(w.shape) for w in self.weights]

                for x, c in minibatch:
                    self.feed_forward(x)

                    e +=  np.mean(self.loss[0](self.y[-1],c))

                    # time.sleep(1)
                    #target = np.zeros(self.y[-1].shape)
                    #target[0,c] = 1
                    self.backpropagation(c) # ar trebui c aici

                eroare += e/minibatch_size
                self.apply_changes(minibatch_size, nr_instante_antrenare)

            print("gata", time.time()-acum, "secunde")
            acuratete.append(self.check_accuracy(validation[0], validation[1]))
            pierderi.append(eroare / nr_minibatchuri)
            # print(self.check_accuracy(train[0], train[1]))
            print(acuratete[-1], pierderi[-1])
            if self.adaptive_lr and epoch > 0:
                if pierderi[epoch] > pierderi[epoch - 1]:
                    beta = 1.05
                else:
                    beta = 0.7
                self.lr *= beta
        
        lista_return = {
            "loss": pierderi,
            "acc": acuratete
        }

        return lista_return
    
    def predict(self, x):
        self.feed_forward(x)
        return self.y[-1]

class CGP:
    """
    CGP (Conjugate Gradient BKP)
    variabile:
        - learning rate
        - momentum
    """

    def __init__(self, lr=0.3, clipnorm=1.0):
        self.lr = lr
        self.clipnorm = clipnorm
        self.z = []
        self.y = []
        

    def update(self, layers, loss, comparing_function):
        """
        se actualizeaza optimizatorul cu straturile retelei
        se initializeaza weighturile si biasurile
        se actualizeaza si functia de cost (pierdere)
        """
        
        self.weights = []
        self.biases = []
        self.layers = layers
        self.loss = losses_dict[loss]
        self.no_layers = len(layers)
        self.l2_regularizers = [l.l2_regularizer for l in layers]
        self.comparing_function = comparing_function

        for i in range(1, self.no_layers):
            layers[i].weight_params["fan_in"] = layers[i-1].no_units
            layers[i].weight_params["fan_out"] = layers[i].no_units
            self.weights.append(initializer_dict[layers[i].weight_initializer](**layers[i].weight_params))
            
            layers[i].bias_params["fan_in"] = 1
            layers[i].bias_params["fan_out"] = layers[i].no_units
            self.biases.append(initializer_dict[layers[i].bias_initializer](**layers[i].bias_params))

        self.delta_L_func = self.loss[2].get(layers[-1].activation[0].__name__, None)
        #print(len(self.biases), self.biases)

    def feed_forward(self, inp):
        """
        pasul de feed forwarding
        y[-1] contine outputul retelei
        """
        self.z.clear()
        self.y.clear()

        self.y.append(inp)
        for i in range(self.no_layers):
            self.z.append(np.dot(self.y[i], self.weights[i]) + self.biases[i])
            self.y.append(self.layers[i].activation[0](self.z[i]))

        
        # print(self.z)

        # print([a.shape for a in self.z], [a.shape for a in self.y])

    def faster_check(self, inp):
        """
        functie asemanatoare feed_forward, fara a stoca rezultatele de pe celelalte straturi
        """
        y = np.copy(inp)

        for i in range(self.no_layers):
            z = np.dot(y, self.weights[i]) + self.biases[i]
            y = self.layers[i].activation[0](z)

        return y


    def backpropagation(self, t):
        """
        pasul de backpropagation
        delta reprezinta modificarea care trebuie aplicata pentru scaderea costului
        se propaga eroarea pentru a actualiza diferentele cu care trebuie modificate
        weighturile si biasurile
        """
        # eroare = self.y[-1] - t
        delta = self.loss[1](self.y[-1], t)*self.layers[-1].activation[1](self.z[-1]) if self.delta_L_func is None \
            else self.delta_L_func(self.y[-1],t)#*self.layers[-1].activation[1](self.z[-1])


        # gradient norm clipping
        norma = np.linalg.norm(delta)
        if norma != 0:
            delta = delta / norma * self.clipnorm

        #print(delta)
        #time.sleep(1)
        # print(delta.shape, self.y[-2].shape)
        self.dif_biases[-1] += delta   
        self.dif_weights[-1] += self.y[-2].T.dot(delta) 
        for i in range(len(self.dif_biases)-2, -1, -1):
            delta = (delta.dot(self.weights[i+1].T))*self.layers[i].activation[1](self.z[i])
            self.dif_biases[i] += delta
            self.dif_weights[i] += self.y[i].T.dot(delta)

        #time.sleep(1)

    def apply_changes(self, minibatch_size, nr_instante_antrenare, first=False):
        """
        se modifica biasurile si weighturile in functie de diferentele calculate
        in pasul de backpropagation


        a = np.array([1,2,3,4]).reshape((1,4))
        b = np.array([15,1,3,2]).reshape((1,4))

        c = a.T.dot(a)
        d = b.T.dot(b)

        print(np.trace(c)/np.trace(d))
        print(np.inner(a,a)/np.inner(b,b))
        print(np.linalg.norm(a)**2/np.linalg.norm(b)**2)
        """


        #print(self.weights)
        #print(self.dif_weights)
        for i in range(len(self.biases)):
            if not first:
                #beta_bias = np.trace((self.dif_biases[i]-self.prec_dif_biases[i]).T.dot(self.dif_biases[i]))/(np.linalg.norm(self.prec_dif_biases[i])**2)
                # polak-ribiere
                #print(np.trace((self.dif_weights[i]-self.prec_dif_weights[i]).T.dot(self.dif_weights[i])))
                #print((np.linalg.norm(self.prec_dif_weights[i])**2))
                beta_weight = max(np.trace((self.dif_weights[i]-self.prec_dif_weights[i]).T.dot(self.dif_weights[i]))/(np.linalg.norm(self.prec_dif_weights[i])**2),0)
                #print("Beta frate", beta_weight)
                # fletcher-reeves
                #beta_weight = np.trace((self.dif_weights[i]).T.dot(self.dif_weights[i]))/(np.linalg.norm(self.prec_dif_weights[i])**2)

                # print(beta_bias, beta_weight,(np.linalg.norm(self.prec_dif_biases[i])**2))
                self.p_bias[i] = -self.dif_biases[i] # + beta_bias*self.p_bias[i]
                self.p_weight[i] = -self.dif_weights[i] + beta_weight*self.p_weight[i]

            else:
                self.p_bias[i] = -self.dif_biases[i] 
                self.p_weight[i] = -self.dif_weights[i]
                
            self.biases[i] += self.p_bias[i] * self.lr / minibatch_size
            self.weights[i] = (1-self.l2_regularizers[i]*self.lr / nr_instante_antrenare)*self.weights[i]+self.p_weight[i] * self.lr / minibatch_size
            
            self.prec_dif_biases[i] = self.dif_biases[i]
            self.prec_dif_weights[i] = self.dif_weights[i]+1e-100 # prevenim zerourile


    def check_accuracy(self, inp, target):
        """
        verificarea acuratetei
        """
        no_validation_instances = len(inp)
        correct = 0
        
        for i in range(no_validation_instances):
            
            y = self.faster_check(inp[i])

            #print(y, target[i])
            #time.sleep(1)
            if self.comparing_function(y, target[i]): # == self.comparing_function(target[i]):
                correct += 1


        return 100*correct / no_validation_instances 


    def train(self, train, validation, no_epochs, minibatch_size, metrics):
        self.layers[0].weight_params["fan_in"] = train[0][0].shape[-1]
        self.layers[0].weight_params["fan_out"] = self.layers[0].no_units
        self.weights.insert(0,initializer_dict[self.layers[0].weight_initializer](**self.layers[0].weight_params))

        self.layers[0].bias_params["fan_in"] = 1
        self.layers[0].bias_params["fan_out"] = self.layers[0].no_units
        self.biases.insert(0,initializer_dict[self.layers[0].bias_initializer](**self.layers[0].bias_params))


        self.prec_dif_biases = [np.zeros(b.shape) for b in self.biases]
        self.prec_dif_weights = [np.zeros(w.shape) for w in self.weights]
        self.p_bias = [np.zeros(b.shape) for b in self.biases]
        self.p_weight = [np.zeros(w.shape) for w in self.weights]
         
        nr_instante_antrenare = len(train[0])

        first = True
        pierderi = []
        acuratete = []

        eroare_prec = 0

        for epoch in range(no_epochs):
            print("Epoca", epoch)
            
            lista_indecsi = list(range(0, nr_instante_antrenare))
            random.shuffle(lista_indecsi)

            minibatchuri = [[(train[0][i], train[1][i]) \
                for i in lista_indecsi[j:j+minibatch_size]] \
                for j in range(0, nr_instante_antrenare, minibatch_size)]


            nr_minibatchuri = len(minibatchuri)

            eroare = 0
            acum = time.time()
            for minibatch in minibatchuri:
                e = 0
                self.dif_biases = [np.zeros(b.shape) for b in self.biases]
                self.dif_weights = [np.zeros(w.shape) for w in self.weights]

                for x, c in minibatch:
                    self.feed_forward(x)
                    # time.sleep(1)
                    #target = np.zeros(self.y[-1].shape)
                    #target[0,c] = 1
                    self.backpropagation(c)
                    e += np.mean(self.loss[0](self.y[-1], c))

                self.apply_changes(minibatch_size, nr_instante_antrenare, first)

                if first:
                    first = False
                # print(self.weights)

                eroare += e/minibatch_size

            eroare /= nr_minibatchuri

            coeficient_lr = 0.7 if eroare > 1.04*eroare_prec else 1.05
            eroare_prec = eroare

            self.lr *= coeficient_lr

            print("gata", time.time()-acum, "secunde")
            acuratete.append(self.check_accuracy(validation[0], validation[1]))
            pierderi.append(eroare)
            # print(self.check_accuracy(train[0], train[1]))
            print(acuratete[-1], pierderi[-1])
            time.sleep(1)
        
        lista_return = {
            "loss": pierderi,
            "acc": acuratete
        }

        return [lista_return[x] for x in metrics]