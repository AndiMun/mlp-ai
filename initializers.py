import numpy as np

"""
Initializatori:
    - lecun normal
    - glorot normal
    - constant
    - zeros
    - ones
"""

def lecun_normal(fan_in, fan_out):
    return np.random.randn(fan_in, fan_out)*np.sqrt(1/fan_in)

def lecun_uniform(fan_in, fan_out):
    limita = (3/fan_in)**(1/2)
    return np.random.rand(fan_in, fan_out)*2*limita-limita

def glorot_normal(fan_in, fan_out):
    return np.random.randn(fan_in, fan_out)*np.sqrt(2/(fan_in+fan_out))

def constant(value, fan_in, fan_out):
    return np.full(shape=(fan_in, fan_out),fill_value=value)

def zeros(fan_in, fan_out):
    return np.zeros(shape=(fan_in,fan_out))

def ones(fan_in, fan_out):
    return np.ones(shape=(fan_in,fan_out))


initializer_dict = {
    'lecun_normal': lecun_normal,
    'glorot_normal': glorot_normal,
    'zeros': zeros,
    'lecun_uniform': lecun_uniform
}