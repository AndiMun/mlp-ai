from model import Model
from layer import Layer
from sgd import SGD, CGP
import numpy as np
import gzip
import pickle
from datasets import FacebookComments, Mnist_digit, convert_number_digits
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import copy
 
""" f = gzip.open('mnist.pkl.gz', 'rb')
train_set, valid_set, test_set = pickle.load(f, encoding="bytes")
f.close()

 """

def creare_numar(repr):
    impartire = [str(np.argmax(repr[i*10:i*10+10])) for i in range(len(repr)//10)]
    return ''.join(impartire)


md = Mnist_digit()
#fb = FacebookComments()

#fb_com = FacebookComments()
#my_train, my_test = fb_com.train_variants[0], fb_com.test_cases[0]
#new_train = [[x.reshape(-1, x.shape[0]) for x in my_train[0]], my_train[1]]
#t = np.eye(10)

#new_train_set = ([x.reshape(-1, x.shape[0]) for x in np.concatenate((train_set[0], valid_set[0]))],
#                np.concatenate((train_set[1], valid_set[1])))
new_train_set, test_set = md.get_validation_data()

epoch_no = 10

a = Layer(100, 'sigmoid', l2_regularizer=10)
m1 = Model()
m1.add_layer(a)
m1.add_layer(Layer(10, 'softmax', l2_regularizer=10))

m2 = copy.deepcopy(m1)
m3 = copy.deepcopy(m1)
m4 = copy.deepcopy(m1)
#m.compile(CGP(lr=0.1),'cross-entropy')
m1.compile(SGD(lr=0.2,momentum=0.9), 'cross-entropy', np.argmax)
a = m1.fit(new_train_set,test_set, epoch_no)
#m.fit(new_train,my_test,10)
m2.compile(SGD(lr=0.2,momentum=0.9, adaptive_lr=True), 'cross-entropy', np.argmax)
b = m2.fit(new_train_set,test_set, epoch_no)

m3.compile(SGD(lr=0.2), 'cross-entropy', np.argmax)
c = m3.fit(new_train_set,test_set, epoch_no)

m4.compile(CGP(lr=0.2), 'cross-entropy', np.argmax)
d = m4.fit(new_train_set,test_set, epoch_no)

plt.plot(list(range(0, len(a['loss']) + 1)), [0] + a['loss'], 'r', label='SGD with momentum')
plt.plot(list(range(0, len(b['loss']) + 1)), [0] + b['loss'], 'b', label='SGD with momentum and adaptive lr')
plt.plot(list(range(0, len(c['loss']) + 1)), [0] + c['loss'], 'y', label='SGD')
plt.plot(list(range(0, len(d['loss']) + 1)), [0] + d['loss'], 'g', label='CGP')

red_patch = mpatches.Patch(color='red', label='SGD with momentum')
blue_patch = mpatches.Patch(color='blue', label='SGD with momentum and adaptive lr')
yellow_patch = mpatches.Patch(color='yellow', label='SGD')
green_patch = mpatches.Patch(color='green', label='CGP')

plt.legend(handles=[red_patch, blue_patch, yellow_patch, green_patch])

plt.yticks([0, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.09, 0.1])
plt.grid(True)

plt.show()
plt.clf()
plt.cla()
plt.close()

plt.plot(list(range(0, len(a['acc']) + 1)), [0] + a['acc'], 'r', label='SGD with momentum')
plt.plot(list(range(0, len(b['acc']) + 1)), [0] + b['acc'], 'b', label='SGD with momentum and adaptive lr')
plt.plot(list(range(0, len(c['acc']) + 1)), [0] + c['acc'], 'y', label='SGD')
plt.plot(list(range(0, len(d['loss']) + 1)), [0] + d['loss'], 'g', label='CGP')

plt.legend(handles=[red_patch, blue_patch, yellow_patch, green_patch])
plt.yticks(list(range(0, 110, 10)))
plt.grid(True)
plt.show()



""" a = np.array([1,2,3,4]).reshape((1,4))
b = np.array([15,1,3,2]).reshape((1,4))

c = a.T.dot(a)
d = b.T.dot(b)

print(np.trace(c)/np.trace(d))
print(np.inner(a,a)/np.inner(b,b))
print(np.linalg.norm(a)**2/np.linalg.norm(b)**2) """