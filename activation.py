import numpy as np

"""
Functiile de activare ale straturilor:
    - sigmoid
    - softmax
"""

def sigmoid(x):
    return 1/(1+np.exp(-x))

def derivative_sigmoid(x):
    s = sigmoid(x)
    return s*(1-s)

def softmax(x):
    maxi = np.max(x)

    expo = np.exp(x-maxi) # discutabil cu maxi
    suma = np.sum(expo)

    return expo / suma        
    

def derivative_softmax(x):
    s = softmax(x)
    return s*(1-s)

def relu(x):
    return np.where(x > 0, x, 0)

def derivative_relu(x):
    return np.where(x > 0, 1, 0)
    
def linear(x):
    return x

def derivative_linear(x):
    return np.ones(x.shape)

def leaky_relu(x):
    return np.where(x > 0, x, 0.1*x)

def derivative_leaky_relu(x):
    return np.where(x > 0, 1, 0.1)


activation_dict = {
    'sigmoid': (sigmoid, derivative_sigmoid),
    'softmax': (softmax, derivative_softmax),
    'relu': (relu, derivative_relu),
    'linear': (linear, derivative_linear),
    'leaky_relu': (leaky_relu, derivative_leaky_relu)
}